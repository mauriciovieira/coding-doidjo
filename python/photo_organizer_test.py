import unittest
from doidjo import PhotoOrganizer, OriginDoesNotExistException
import datetime
import os
import shutil

class PhotoOrganizerTest(unittest.TestCase):
	"""
	def testGetMetadata(self):
		po = PhotoOrganizer(file="foto1.jpg")
		today = datetime.datetime(2014, 5, 10)
		self.assertEquals(po.date,today)
	"""
	def setUp(self):
		if not os.path.exists("origem"):
			os.makedirs("origem")

		self.po = PhotoOrganizer("origem", "destino")


	def tearDown(self):
		if os.path.isdir("destino"):
			shutil.rmtree("destino")			

	def testCreateDirectory(self):
		self.po.create_directory("2014/05/10")
		self.assertTrue(os.path.isdir("destino/2014/05/10"))

	def testOriginExists(self):
		po = PhotoOrganizer("origem_inexistente","destino")
		self.assertRaises(OriginDoesNotExistException, po.load_files)

	def testLoadFiles(self):
		self.po.load_files()
		self.assertNotEquals(len(self.po.files), 0)


if __name__ == '__main__': 
    unittest.main()