import os

class PhotoOrganizer:
	def __init__(self, origin, destination):
		"""source directory 
		destination directory"""
		self.origin = origin
		self.destination = destination
		self.files = []

	def create_directory(self,path):
		final_path = os.path.join(self.destination, path)
		if not os.path.exists(final_path):
			os.makedirs(final_path)

	def load_files(self):
		array_files = []
		if not os.path.exists(self.origin):
			raise OriginDoesNotExistException()
		
		for root, dirs, files in os.walk(self.origin):
		    for file in files:
		    	if file.endswith((".jpg", ".jpeg", ".png", ".gif")):
		             array_files += os.path.join(root, file)
		self.files= array_files


class OriginDoesNotExistException(Exception):
	pass