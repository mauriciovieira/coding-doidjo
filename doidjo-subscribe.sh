#!/bin/sh

if [[ ! -d .coding-doidjo ]]
then
   echo "Create a .coding-doidjo directory first"
   exit 1
fi

nickname=$1
name=$2
email=$3

echo ${nickname}:${name}:${email} >> .coding-doidjo/doidjos.txt

