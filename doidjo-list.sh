#!/bin/sh

if [[ ! -d .coding-doidjo ]]
then
   echo "Create a .coding-doidjo directory first"
   exit 1
fi

echo "Doidjos" 
cat .coding-doidjo/doidjos.txt | sed 's/:/ 	 /g'
