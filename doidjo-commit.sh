#!/bin/sh

if [[ ! -d .coding-doidjo ]]
then
   echo "Create a .coding-doidjo directory first"
   exit 1
fi

nickname=$1

line=$(grep $nickname .coding-doidjo/doidjos.txt)
if [[ ${line}x == "x" ]]
then
  echo "$nickname not found. Use doidjo-subscribe.sh to subscribe a new developer"
  exit 2
fi

name=$(echo $line | cut -f2 -d:)
email=$(echo $line | cut -f3 -d:)

git add -A
git commit --author="$name <${email}>"
